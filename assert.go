package main

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

func getTestCase(outdir, testCase string) (string, int, error) {
	tetRoot := env("TET_ROOT", "/usr/local/share")
	tetScen := filepath.Join(tetRoot, "xts5", "tet_scen")
	in, err := os.Open(tetScen)
	if err != nil {
		return "", 0, err
	}
	defer in.Close()

	s := bufio.NewScanner(in)
	for s.Scan() {
		line := s.Text()
		if line == testCase {
			num := 0
			for s.Scan() {
				line := s.Text()
				switch {
				case strings.Contains(line, "CASE"):
					f := strings.Fields(line)
					if len(f) < 1 {
						break
					}
					count := f[len(f)-1]
					if !strings.HasSuffix(count, `"`) {
						break
					}
					n, err := strconv.Atoi(count[:len(count)-1])
					if err == nil {
						num = n
					}
				case strings.Contains(line, "/"):
					return strings.TrimSpace(line), num, nil
				}
			}
			break
		}
	}
	return "", 0, s.Err()
}

func runAssert(outdir string) (int, error) {
	testCase := flag.Arg(0)
	var assertions []int
	for _, v := range flag.Args()[1:] {
		a, err := strconv.ParseInt(v, 10, 16)
		if err != nil {
			return 1, err
		}
		assertions = append(assertions, int(a))
	}
	if len(assertions) == 0 {
		return 1, errors.New("no assertion(s) were specified")
	}

	testApp, num, err := getTestCase(outdir, testCase)
	if err != nil {
		return 1, err
	}
	if testApp == "" {
		return 1, errors.New("cannot find test " + testCase)
	}

	aScen := filepath.Join(outdir, "assert_scen")
	out, err := os.Create(aScen)
	if err != nil {
		return 1, err
	}
	defer out.Close()

	fmt.Fprintln(out, testCase)
	fmt.Fprintf(out, "  \"VSW5TESTSUITE CASE %s %d\"\n", testCase, num)
	for _, a := range assertions {
		if a <= 0 || a > num {
			return 1, fmt.Errorf("%s does not contain %d tests", testCase, a)
		}
		fmt.Fprintf(out, "  %s{%d}\n", testApp, a)
	}

	args := []string{"-e", "-i", outdir, "-x", *config, "-s", aScen, "xts5", testCase}
	return tcc(args)
}
